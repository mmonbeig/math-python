# Partie 1 : Commandes avancées Git

1. *RAS*
1. Le bogue se situe dans celui nommé "tesTFibonacci". Comme il y a un T majuscule, python ne le détecte pas et ne l'éxecute pas. Une fois que le texte s'exécute on peut voir qu'il y a un problème à résoudre avec la fonction
1. *RAS*
1. *RAS*
1. *RAS*
1. Le mauvais commit pour le test est le commit "Add tests for fibonacci function". Le mauvais commit pour la fonction `fibonacci` est le commit "Replace i index with more explicit loop_count".
1. L'auteur de la ligne 47 du fichier `functions.py` est Guillaume Bernard, et cette ligne a été modifiée pour la dernière fois au commit "db92b1d8" qui correspond au commit "Replace i index with more explicit loop_count".
1. *RAS*
1. *RAS*
1. Ce fichier correspond à un résumé des modifications effectués lors du commit qui a été utilisé pour le patch.

# Partie 2 : Collaboration de code avec Gitlab

1. Binôme : Maxime GODELAIN
1. *RAS*
1. *RAS*
1. Création de 2 branches distinctes
1. Création de 2 *Merge Requests* reliées aux branches créées
1. Retour sur chacunes des *Merge Requests*
1. Règles dans le fichier `CONTRIBUTING.md`
1. *RAS*
1. *RAS*

# Partie 3 : Utilisation de `pre-commit` pour Python

1. Ajout de `pre-commit` dans `requirements-dev.txt`
1. Malheureusement avec isort mon fichier ne fonctionnait pas, je me suis rabattu sur black seulement et les propositions de la commande `pre-commit sample-config`.
1. Certains fichiers étaient déjà mal formattés
1. `pre-commit run --all-files`
1. *RAS*
1. *RAS*
