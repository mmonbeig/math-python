# Règles de bonnes pratiques

- Commenter toutes les fonctions avec :
  - une brève description de la fonction
  - les paramètres d'entrée et leur utilisation
  - le retour de la fonction
- Faire des tests en adéquation avec les différents cas de la fonction
