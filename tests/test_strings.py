from unittest import TestCase

from math2.strings import levenshteinDistance, anagramme


class TestString(TestCase):
    def test_levenshtein(self):
        self.assertEqual(3, levenshteinDistance("kitten", "sitting"))
        self.assertEqual(8, levenshteinDistance("rosettacode", "raisethysword"))

    def test_anagramme(self):
        self.assertCountEqual(
            [
                "kucd",
                "kudc",
                "kcud",
                "kcdu",
                "kduc",
                "kdcu",
                "ukcd",
                "ukdc",
                "uckd",
                "ucdk",
                "udkc",
                "udck",
                "cukd",
                "cudk",
                "ckud",
                "ckdu",
                "cduk",
                "cdku",
                "dkuc",
                "dkcu",
                "dukc",
                "duck",
                "dcuk",
                "dcku",
            ],
            anagramme("duck"),
        )
        self.assertCountEqual(["aaa"], anagramme("aaa"))
